from flask import render_template
from paper import app

with  app.test_request_context() :
      print render_template('mail.txt',user='aa', table='table')
      print render_template('mail.html',user='aa', titles=['PMID','TITLE'], papers=[{'PMID':12, 'TITLE':'ok', 'PMID':30, 'TITLE':'another'}])
