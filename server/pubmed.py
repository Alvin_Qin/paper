import re
import traceback
from Bio import Entrez
from Bio import Medline
from subprocess import call
from operator import itemgetter, attrgetter
import sys, os

from werkzeug.routing import BaseConverter
from flask import jsonify, current_app, render_template
from flask.ext.mail import Message
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders

from flask import copy_current_request_context
from flask import request
from flask.ext.captcha.models import CaptchaStore

import time

import urllib2, requests
import socket
import json
import time

def getIF(paper,IFlist):
    import cPickle as p
    f = file(IFlist)
    paper = str(paper).strip().upper()
    IFlist = p.load(f)
    f.close()
    return IFlist.get(paper, 0)

def PubMed(IFlist):
    t0 = time.time()
    term, mail, impact, top = 'CRISPR[ti]', '1410771@tongji.edu.cn', 5, 100
    limit = 200

    Entrez.email = mail
    Papers = []
    batch_size = 500
    ## usehistory will iterative increasing records number
    search = Entrez.esearch(db="pubmed", term=term, usehistory="y")
    record = Entrez.read(search)

    count = int(record["Count"]) 
    idlist=record["IdList"]
    search.close()
    webenv = record["WebEnv"] # use with usehistory
    query_key = record["QueryKey"]

    def entrez_parse(args):
        start, batch_size = args
        papers = []
        try:
            handle = Entrez.efetch(db="pubmed", rettype="medline", retmode="text", retstart = start, retmax=batch_size, webenv=webenv, query_key=query_key)
            for record in Medline.parse(handle):
                paper = fetch_record((record, IFlist, impact))
                if paper:
                    papers.append(paper)
            handle.close()
        except:
                # time.sleep(0.5)
                handle = Entrez.efetch(db="pubmed", rettype="medline", retmode="text", retstart = start, retmax=batch_size, webenv=webenv, query_key=query_key) 
                for record in Medline.parse(handle):
                    paper = fetch_record((record, IFlist, impact))
                    if paper:
                        papers.append(paper)
                handle.close()
        return papers

    # p = Pool(4)
    batches = range(0, count, batch_size)
    Papers = []

    ##for pr in p.imap(entrez_parse, zip(batches, [batch_size]*len(batches))): ## greenlet version not stable
    for b in batches: ## no greenlet, stable
        pr = entrez_parse((b, 500))
        if pr:
           Papers += pr

    Papers_sorted=sorted(Papers,key=lambda x:(x['DAF'], x['IF']),reverse=True)
    outMsg = ""
    leftPaper = len(Papers_sorted)

    if str(top).strip().upper() != 'ALL':
        results = Papers_sorted[:int(top)]
    else:
        results = Papers_sorted

    batches = range(0, len(results), 25) # every 25 records

    for p in results:
        doi = finddoi(p.get('LID', ''))
        if doi:
            p['DOI'] = 'DOI(' + str(doi) + ')'
            ## no DOI, use pubmed iid
        else:
            p['DOI'] = 'PMID(' + str(p['PMID']) + ')'

    DOI = [ p.get('DOI','') for p in results if p.has_key('DOI') ]
    DOI = '+OR+'.join(DOI)
    citations = []
    for start in range(0, len(results), 10):
        req = urllib2.Request('http://api.elsevier.com/content/search/index:SCOPUS?start=%s&count=10&query=%s&field=citedby-count,pubmed-id,prism:doi' % (start,DOI))
        req.add_header('X-ELS-APIKey', '1e5ef7a4e5fa55b80b227711673d9411')
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        try:
            r = opener.open(req)
            c = r.read()
            citations += json.loads(c)['search-results']['entry']
            r.close()
        except:
            continue
    try:
        cite_dict = {}
        for ci in citations:
            try:
                cite_dict[ci['pubmed-id']] = [ci['citedby-count'], ci['prism:url']]
            except:
                cite_dict[ci['prism:doi']] = [ci['citedby-count'], ci['prism:url']]
        for P in results:
            if cite_dict.has_key(str(P['PMID'])):
                P['cite'] =  cite_dict[str(P['PMID'])][0]
                P['scp'] = cite_dict[str(P['PMID'])][1].split(':')[-1]

    except  Exception, e:
        exstr = traceback.format_exc()
        print exstr

    print results

def fetch_record(args):
    record, IFlist, impact = args
    TA = record.get("TA", "NA")
    IF = getIF(TA, IFlist)
    authors = record.get("AU", "NA")
    if authors == "NA":
        first_author = "NA"
        last_author = "NA"
    else:
        first_author = authors[0]
        last_author = authors[-1]
    paper = record.get("TI", "NA")
    #querier = ScholarQuerier()
    #settings = ScholarSettings()
    #querier.apply_settings(settings)
    #query = SearchScholarQuery()
    #query.set_author(first_author)
    #query.set_phrase(paper)
    #querier.send_query(query)
    cite_num = 0
    #for art in querier.articles:
    #    cite_num = int(encode(art['num_citations']))
    #querier.save_cookies()
    if IF>=impact:
        return {'LID': record.get('LID', ''), 'PMID': record.get('PMID', "NA"), 'TI': record.get("TI", "NA"), 'TA': TA, 'first_author': first_author, 'last_author':last_author, 'DA': str(record.get("DA", "NA"))[:-4], 'IF': int(IF), 'DAF': int(int(int(record.get("DA", 0))/10000)+IF), 'cite': cite_num}
    return 0


def finddoi(content):
    if not 'doi' in content:
        return False
    regex = [r'\] (.*) \[doi\]', r'^(\d.*) \[doi\]']
    for r in regex:
        m = re.findall(r, content)
        if m:
            return m[0]
    return False

PubMed('2012-if.data')
