#!/usr/bin/env python
ifdata = {}
with open("2012-JCR2011-SCI-IF.txt") as inf:
    fin = inf.read().split('\r')
    impact = 0
    for line in fin:
        if '5-Year' in line: continue
        line = line.strip().split("\t")
        if line[4]: # 2012 if
            impact = line[4].strip()
        if line[5]: # 5 year if
            impact = line[5].strip()
        if impact:
            ifdata[line[1].strip().upper()] = float(impact)
        else:
            ifdata[line[1].strip().upper()] = 0

f = file('2012-if.data', 'w')
import cPickle as p
p.dump(ifdata, f)
f.close()
