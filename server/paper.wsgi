import sys
import os
sys.stdout = sys.stderr
project = '/data/home/qqin/paper/server/'

activate_this = os.path.join(project, 'venv','bin', 'activate_this.py')
execfile(activate_this, dict(__file__=activate_this))
sys.path.append(project)

from paper import app as application
