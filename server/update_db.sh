#!/bin/bash

cp flask-captcha/flask_captcha/fonts/Vera.ttf venv/lib/python2.7/site-packages/Flask_Captcha-0.1.8-py2.7.egg/flask_captcha/fonts/
# change flask-captcha settings.py 
# ROOT/venv/local/lib/python2.7/site-packages/Flask_Captcha-0.1.8-py2.7.egg/flask_captcha/settings.py

#cd /mnt/Storage/home/qinq/03_Completed/gnosis
cd /data/home/qqin/paper/server
source venv/bin/activate
python run.py create_tables

#mkdir -p /var/www/.config/biopython/Bio/Entrez/DTDs
#sudo chown -R qqin  /var/www/.config && chgrp -R lab /var/www/.config
#wget http://effbot.org/downloads/Imaging-1.1.7.tar.gz


touch gnosis_p.log && chmod 777 gnosis_p.log
