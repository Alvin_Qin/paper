from flask.ext.script import Manager

from paper import app
from flask.ext.captcha.models import CaptchaStore
from paper import db

# By default, Flask-Script adds the 'runserver' and 'shell' commands to
# interact with the Flask application. Add additional commands using the
# `@manager.command` decorator, where Flask-Script will create help
# documentation using the function's docstring. Try it, and call `python
# manage.py -h` to see the outcome.

manager = Manager(app)

@manager.command
def create_tables():
    " Create relational database tables"
    with app.app_context():
        #print("Creating db..")
        db.create_all()
        from flask.ext.captcha.helpers import init_captcha_dir, generate_images

        init_captcha_dir()
        count = generate_images(app.config['CAPTCHA_PREGEN_MAX'])
        print("created %s captchas" % count)
        values = CaptchaStore.get_all()

@manager.command
def list_tables():
    print CaptchaStore.get_all()
    #values = CaptchaStore.query.filter(CaptchaStore.hashkey==u'bd0e11e196ca2b1d6ffc471e27e479856321bd9a').first()
    #values = CaptchaStore.query.filter(CaptchaStore.hashkey==u'ze11e196ca2b1d6ffc471e27e479856321bd9a').first()
    #print values

@manager.command
def delete_tables():
    with app.app_context():
        #print("Deleting db..")
        from flask.ext.captcha.helpers import clear_images
        deleted = CaptchaStore.delete_all()
        clear_images()

if __name__ == '__main__':
    manager.run()
