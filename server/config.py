import os
DEBUG = False

PRESERVE_CONTEXT_ON_EXCEPTION = False
DEBUG_TB_INTERCEPT_REDIRECTS = True
DEBUG_TB_PROFILER_ENABLED = True

BCRYPT_LEVEL = 12

CACHE_TYPE = 'simple' # null
# CACHE_TYPE = 'null'
## IF = '/data/home/qqin/server/2012-JCR2011-SCI-IF.txt'
#IF = '/mnt/Storage/home/qinq/03_Completed/gnosis/2012-if.data'
IF = '/data/home/qqin/paper/server/2012-if.data'

MAIL_SERVER = 'localhost'
MAIL_USERNAME = 'pr@cistrome.org'
MAIL_DEFAULT_SENDER = 'pr@cistrome.org'

#MAIL_SERVER='smtp.gmail.com'
#MAIL_SERVER = 'smtp.tongji.edu.cn'
#MAIL_PORT=465
#MAIL_USE_TLS = False
#MAIL_USE_SSL= True

CAPTCHA_PREGEN_MAX=500
CAPTCHA_FONT_PATH = '/data/home/qqin/paper/server/flask-captcha/flask_captcha/fonts/Vera.ttf'

#SQLALCHEMY_DATABASE_URI = 'sqlite://///mnt/Storage/home/qinq/03_Completed/gnosis/paper.sqlite'
SQLALCHEMY_DATABASE_URI = 'sqlite://///data/home/qqin/paper/server/paper.sqlite'

