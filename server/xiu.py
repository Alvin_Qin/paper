
#from pyquery import PyQuery as pyq
from urllib2 import urlopen
from bs4 import BeautifulSoup
import time
import os
from socket import error as SocketError
import errno
from gevent import monkey; monkey.patch_all()
import gevent
from gevent import pool
g = pool.Pool(20)

def urlparse(url):
    try:
        response = urlopen(url)
        bt = BeautifulSoup(response.read())
        response.close()
        time.sleep(0.5)
    except SocketError as e:
        if e.errno != errno.ECONNRESET:
            raise e.errno # Not error we are looking for
        time.sleep(0.5)
        response = urlopen(url)
        bt = BeautifulSoup(response.read())
        response.close()
    return bt

def gevent_func(args):
    u, d = args
    dicts = {}
    dicts[d] = {}
    dicts[d]['OMIM'] = 'NA'
    dicts[d]['Orphanet'] = 'NA'
    other_link = urlparse(u)
    for link in other_link.find_all(class_ = "cnt_list"):
        for linka in link('a'):
            if 'OMIM' in linka.get_text():
               dicts[d]['OMIM'] = linka.get('href').split('/')[-1]
            if 'Orphanet' in linka.get_text():
               dicts[d]['Orphanet'] = linka.get('href').split('=')[-1]
    return dicts

def func(url, disease):
    dicts = {}
    for u, d in zip(url, disease):
        dicts[d] = {}
        dicts[d]['OMIM'] = 'NA'
        dicts[d]['Orphanet'] = 'NA'
        other_link = urlparse(u)
        for link in other_link.find_all(class_ = "cnt_list"):
            for linka in link('a'):
                if 'OMIM' in linka.get_text():
                   dicts[d]['OMIM'] = linka.get('href').split('/')[-1]
                if 'Orphanet' in linka.get_text():
                   dicts[d]['Orphanet'] = linka.get('href').split('=')[-1]
    return dicts

root = urlparse("http://rarediseases.info.nih.gov/gard/categories")

rootUrl = "http://rarediseases.info.nih.gov"
categories = []
dirs = []
for a in root.find_all('a'):
    if a.get('href'):
        if 'diseases-by' in a.get('href'):
            categories.append(rootUrl + a.get('href'))
            dir = a.get('href').split('/')[-1]
            dirs.append(dir)
            #os.mkdir(dir)

for c, d in zip(categories, dirs):
    os.chdir(d)
    categoryContent = urlparse(c)
    fid = open(d+".id", 'w')
    id_urls = []
    disease = []
    for a in categoryContent.find_all(class_="DiseaseList"):
        td_tmp = []
	if a.a:
           if 'resource' in a.a.get('href'):
               #if not (os.path.exists(a.a.get('href').split('/')[-3]) and os.path.getsize(a.a.get('href').split('/')[-3])>0):
               #    syndrome_url = urlparse(rootUrl + a.a.get('href').rstrip('1') + '9')
               #    f = open(a.a.get('href').split('/')[-3], 'w')
               #    n=0
               #    for td in syndrome_url('td'):
               #            n+=1
               #            td_tmp.append(td.get_text())
               #            if n%2==0:
               #               print >>f, '\t'.join(td_tmp)
               #               td_tmp = []
               #    f.close()
               id_urls.append(rootUrl + a.a.get('href'))
               disease.append(a.a.get('href').split('/')[-3])

    #content = func(id_urls, disease) ## d: dir is category information
    #print >>fid, 'disease\tomim\torp'
    #for k, v in  content.iteritems():
    #    print >>fid, k+'\t'+v['OMIM']+'\t'+v['Orphanet']
    #fid.close()

    #geventinputs = map(lambda x: g.spawn(gevent_func, x), zip(id_urls, disease))
    #gevent.joinall(geventinputs)
    #geventinputs[0].value ## not work, use map, imap or imap_unordered
    print >>fid, 'disease\tomim\torp'
    for di in g.imap(gevent_func, zip(id_urls, disease)):
        for k, v in di.iteritems():
            print >>fid, k+'\t'+v['OMIM']+'\t'+v['Orphanet']
    fid.close()

    os.chdir('..')
