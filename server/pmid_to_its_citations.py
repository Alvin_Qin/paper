#!/usr/bin/env python
'''
https://www.biostars.org/p/89478/

given a pubmed ID for an article, I would like to have all the pubmed ID's of the articles that article cites.
One of the returned references, 10827456, is a TIGS paper, which is not in PMC.

Not all references were returned. But as far as I could tell, those references that were not returned were references to book chapters & articles that are not indexed in pubmed.
'''
import sys
from Bio import Entrez as ez
ez.email = "your@emailhere.com"
def get_citations(pmid):
    """
    Returns the pmids of the papers this paper cites
    """
    cites_list = []
#    handle = ez.efetch("pubmed", id=pmid, retmode="xml")
#    pubmed_rec = ez.parse(handle).next()
#    for ref in pubmed_rec['MedlineCitation']['CommentsCorrectionsList']: ## given a paper, fetch its cited paper
#        if ref.attributes['RefType'] == 'Cites':
#            cites_list.append(str(ref['PMID']))

##  given a paper, fetch those papers which cited it
    results = ez.read(ez.elink(dbfrom="pubmed", db="pmc", LinkName="pubmed_pmc_refs", from_uid=pmid))
    pmc_ids = [link["Id"] for link in results[0]["LinkSetDb"][0]["Link"]]
    print len(pmc_ids)   ## citationnumber

    results2 = ez.read(ez.elink(dbfrom="pmc", db="pubmed", LinkName="pmc_pubmed", from_uid=",".join(pmc_ids)))
    pubmed_ids = [link["Id"] for link in results2[0]["LinkSetDb"][0]["Link"]]
    print len(pubmed_ids) ## less than pmc ids 
    print pubmed_ids

if __name__ == '__main__':
    z = get_citations(sys.argv[1])
	#14630660
	#19304878
