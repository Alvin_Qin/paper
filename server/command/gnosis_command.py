#!/usr/bin/env python

from Bio import Entrez
from Bio import Medline
from subprocess import call
from operator import itemgetter, attrgetter
import re
import traceback
import sys, os

import time
import urllib2, requests
import json

def getIF(paper,IFlist):
    import cPickle as p
    f = file(IFlist)
    paper = str(paper).strip().upper()
    IFlist = p.load(f)
    f.close()
    return IFlist.get(paper, 0)

def fetch_record(args):
    record, IFlist, impact = args
    TA = record.get("TA", "NA")
    IF = getIF(TA, IFlist)
    authors = record.get("AU", "NA")

    impact = 3 ## fixed for test, comment later
    paper = record.get("TI", "NA")
    cite_num = 0

    if IF>=impact:
        return {'PMID': record.get('PMID', "NA"), 'TI': record.get("TI", "NA"), 'TA': TA, 'author': authors, 'DA': str(record.get("DA", "NA"))[:-4], 'IF': int(IF), 'DAF': int(int(int(record.get("DA", 0))/10000)+IF), 'cite': cite_num}
    return 0

def PubMed(IFlist, term):
    t0 = time.time()

    Entrez.email = 'test@gmail.com'
    Papers = []
    batch_size = 500
    ## usehistory will iterative increasing records number
    search = Entrez.esearch(db="pubmed", term=term, usehistory="y")
    record = Entrez.read(search)

    count = int(record["Count"]) 
    idlist=record["IdList"]
    search.close()
    webenv = record["WebEnv"] # use with usehistory
    query_key = record["QueryKey"]

    def entrez_parse(args):
        start, batch_size = args
        papers = []
        try:
            handle = Entrez.efetch(db="pubmed", rettype="medline", retmode="text", retstart = start, retmax=batch_size, webenv=webenv, query_key=query_key)
            for record in Medline.parse(handle):
                paper = fetch_record((record, IFlist, 0))
                if paper:
                    papers.append(paper)
            handle.close()
        except:
            handle = Entrez.efetch(db="pubmed", rettype="medline", retmode="text", retstart = start, retmax=batch_size, webenv=webenv, query_key=query_key) 
            for record in Medline.parse(handle):
                paper = fetch_record((record, IFlist, 0))
                if paper:
                    papers.append(paper)
            handle.close()
        return papers

    batches = range(0, count, batch_size)
    Papers = []

    for b in batches:
        pr = entrez_parse((b, 500))
        if pr:
           Papers += pr

    Papers_sorted=sorted(Papers,key=lambda x:(x['DAF'], x['IF']),reverse=True)
    outMsg = ""
    leftPaper = len(Papers_sorted)
    results = Papers_sorted
    for p in results:
        try:
            citation_handler = Entrez.read(Entrez.elink(dbfrom="pubmed", db="pmc", LinkName="pubmed_pmc_refs", from_uid=p['PMID']))
            pmc_ids = [link["Id"] for link in citation_handler[0]["LinkSetDb"][0]["Link"]]
            p['cite'] = len(pmc_ids)
        except:
            p['cite'] = 0

    outMsg = '\t'.join(['PMID', 'Title', 'Journal', 'Author', 'Date', 'Impact Factor', 'Date + Impact Factor', 'Citation']) + '\n'

    for line in results:
        outMsg += '\t'.join([line['PMID'], line['TI'], line['TA'], ','.join(line['author']),line['DA'], str(line['IF']), str(line['DAF']), str(line['cite'])]) + '\n'
    f = open(term+'.txt', 'w')
    f.write('#'+','.join([term,str(count),str(time.time() - t0)]) + '\n')
    f.write(outMsg)
    f.close()

if __name__ == '__main__':
    import sys
    import os

    if os.path.exists(sys.argv[2]+'[tiab].txt'):
        pass
    else:
        PubMed(sys.argv[1], sys.argv[2])
