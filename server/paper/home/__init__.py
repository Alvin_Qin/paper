from flask import Blueprint, current_app
import sys
from flask import request

from ..utils import PubMed
from ..cache import cache
#from ..captcha import getCaptcha

home = Blueprint('home', __name__)

# @home.route('/api/<list:term>', methods=['GET'])
@home.route('/api', methods=['POST', 'GET'])
# @cache.cached(timeout=80) ## cache would cause flask context error, maybe timeout too long
def search_list():
    if request.method == "POST":
        return PubMed(current_app.config['IF'], request)
    return 'methods not right'
