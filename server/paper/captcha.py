from wheezy.captcha.image import captcha, background, curve, noise, smooth, text, offset, rotate, warp
import random
import string

def getCaptcha(handle):
    captcha_image = captcha(drawings = [
    background(),
    text(fonts = ['/usr/share/fonts/truetype/ttf-liberation/LiberationMono-Bold.ttf', '/usr/share/fonts/truetype/ttf-liberation/LiberationMono-Italic.ttf'],
    drawings = [warp(), rotate(), offset()]),
    curve(),
    noise(),
    smooth()])
    info = random.sample(string.uppercase + string.digits, 3)
    print info
    im = captcha_image(info)
    im.save(handle, 'PNG', quality=70)
    return {'secret': info}

getCaptcha()
