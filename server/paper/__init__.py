import logging

from flask import Flask, render_template
from flask.ext.mail import Mail
from flask.ext.sqlalchemy import SQLAlchemy

from home import home
from cache import cache

from flask.ext.captcha import Captcha
from flask.ext.captcha.views import captcha_blueprint

from flask.ext.captcha.models import CaptchaStore, CaptchaSequenceCache

app = Flask(__name__, instance_relative_config = True)

app.config.from_object("flask.ext.captcha.settings")
app.config.from_object('config')
# app.config.from_pyfile('config.py')
# app.config.from_envvar('APP_CONFIG_FILE')

from .utils import ListConverter
from .utils import PubMed

import logging
from logging.handlers import RotatingFileHandler

app.url_map.converters['list'] = ListConverter

if not app.debug:
    #app.logger.addHandler(logging.StreamHandler()) # Log to stderr.
    app.logger.setLevel(logging.INFO)
    handler = RotatingFileHandler('/data/home/qqin/paper/server/gnosis_p.log', maxBytes=10000000, backupCount=20)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s: %(message)s")
    handler.setLevel(logging.INFO)
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)
else:
    # not work
    from flask_debugtoolbar import DebugToolbarExtension
    toolbar = DebugToolbarExtension(app)

@app.errorhandler(500)
def internal_error(exception):
    app.logger.exception(exception)
    return "Sorry internal program error", 500

@app.route("/index")
def captcha_inc():
    val = CaptchaSequenceCache.get().current()

    response = make_response(str(val))
    response.content_type = 'text/plain'
    return response

app.register_blueprint(home)
cache.init_app(app)
mail = Mail(app)

app_captcha = Captcha()
app.register_blueprint(captcha_blueprint, url_prefix='/captcha')
db = SQLAlchemy(app)
app_captcha.init_app(app)

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response
