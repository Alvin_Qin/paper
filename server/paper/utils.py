#!/usr/bin/env python

from Bio import Entrez
from Bio import Medline
from subprocess import call
from operator import itemgetter, attrgetter
import re
import traceback

import sys, os

from werkzeug.routing import BaseConverter
from flask import jsonify, current_app, render_template
from flask.ext.mail import Message
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders

from flask import copy_current_request_context
from flask import request
from flask.ext.captcha.models import CaptchaStore

import time

import urllib2, requests
import socket
import json
# from .scholar import ScholarQuerier, ScholarSettings, SearchScholarQuery

def encode(s):
    if isinstance(s, basestring):
        return s.encode('utf-8') # pylint: disable-msg=C0103
    else:
        return str(s)

# import gevent.monkey
# gevent.monkey.patch_all()
# # gevent.monkey.patch_socket()

# import gevent
# from gevent.pool import Pool

class ListConverter(BaseConverter):

    def to_python(self, value):
        return ' '.join(value.split('+'))

    def to_url(self, values):
        return '+'.join(BaseConverter.to_url(value) for value in values)

def getIF(paper,IFlist):
    import cPickle as p
    f = file(IFlist)
    paper = str(paper).strip().upper()
    IFlist = p.load(f)
    f.close()
    return IFlist.get(paper, 0)

def send_other_email(subject, recipients, text_body, html_body):
    m = current_app.extensions['mail']
    msg = Message(subject, sender=m.default_sender, recipients=[recipients])
    msg.body = text_body
    msg.html = html_body
    @copy_current_request_context
    def send_async_mail():
        m.send(msg)
    sp = gevent.spawn(send_async_mail)
    gevent.joinall([sp])

def send_localhost_mail(resultOpt, subject, to, text, html, attachment, server="localhost"):
    msg = MIMEMultipart('alternative')
    fro = 'gnosis@cistrome.org'
    msg['From'] = fro
    msg['To'] = COMMASPACE.join(to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = 'Gnosis Result' #subject
 
    if resultOpt == 'html':
        msg.attach( MIMEText(html, 'html') )
    else:
        # msg.attach( MIMEText(text, 'plain') )
        import tempfile
        try:
           temp = tempfile.TemporaryFile()
           temp.write(attachment)
           temp.seek(0)
           part = MIMEBase('application', "octet-stream")
           part.set_payload( temp.read() )
           Encoders.encode_base64(part)
           part.add_header('Content-Disposition', 'attachment; filename="%s"'
                           % subject + ".xls")
           msg.attach(part)
        finally:
           temp.close()
    smtp = smtplib.SMTP(server)
    smtp.sendmail(fro, to, msg.as_string() )
    smtp.close()

def fetch_record(args):
    record, IFlist, impact = args
    TA = record.get("TA", "NA")
    IF = getIF(TA, IFlist)
    authors = record.get("AU", "NA")
    if authors == "NA":
        first_author = "NA"
        last_author = "NA"
    else:
        first_author = authors[0]
        last_author = authors[-1]

    # impact = 0 ## fixed for test, comment later
    paper = record.get("TI", "NA")
    #querier = ScholarQuerier()
    #settings = ScholarSettings()
    #querier.apply_settings(settings)
    #query = SearchScholarQuery()
    #query.set_author(first_author)
    #query.set_phrase(paper)
    #querier.send_query(query)
    cite_num = 0
    #for art in querier.articles:
    #    cite_num = int(encode(art['num_citations']))
    #querier.save_cookies()
    if IF>=impact:
#        return {'DOI': finddoi(record.get('LID', '')), 'PMID': record.get('PMID', "NA"), 'TI': record.get("TI", "NA"), 'TA': TA, 'first_author': first_author, 'last_author':last_author, 'DA': str(record.get("DA", "NA"))[:-4], 'IF': int(IF), 'DAF': int(int(int(record.get("DA", 0))/10000)+IF), 'cite': cite_num}
        return {'PMID': record.get('PMID', "NA"), 'TI': record.get("TI", "NA"), 'TA': TA, 'first_author': first_author, 'last_author':last_author, 'DA': str(record.get("DA", "NA"))[:-4], 'IF': int(IF), 'DAF': int(int(int(record.get("DA", 0))/10000)+IF), 'cite': cite_num}
    return 0

def finddoi(content):
    if not 'doi' in content:
        return False
    regex = [r'\] (.*) \[doi\]', r'^(\d.*) \[doi\]']
    for r in regex:
        m = re.findall(r, content)
        if m:
            return m[0]
    return False

def PubMed(IFlist, request):
    t0 = time.time()
    data = request.get_json()
    # use flask-captcha
    resultOpt = data.get('opt', '')
    capt = CaptchaStore.query.filter(CaptchaStore.hashkey==data.get('hash','')).first()
    if not capt:
        return jsonify({'valid':False})
    if str(data.get('recap','')).upper() != str(capt.challenge).upper():
        return jsonify({'valid':False})
    # this is for angular-recaptcha plugin, hard to customize, obsolete
    #recap = data.get('recap','')
    #secret = current_app.config['RECAP_KEY']
    #userdata = {"response":recap, "secret": secret}
    #rq = requests.post("https://www.google.com/recaptcha/api/siteverify", params=userdata)
    #if not rq.json().get('success'):
    #    return jsonify({'valid':False})
    term, mail, impact, top = data.get('keys', ''), data.get('email',''), float(data.get('impact', 0)), int(data.get('top', 1))
    limit = data.get('limit', '')
    parameters = 'term: ' + term + ', impact: ' + str(impact) + ', top paper number' + str(top) + ', up limit: ' + limit

    limit = int(limit.strip('K')) * 1000
    Entrez.email = mail
    Papers = []
    batch_size = 500
    ## usehistory will iterative increasing records number
    search = Entrez.esearch(db="pubmed", term=term, usehistory="y")
    record = Entrez.read(search)

    count = int(record["Count"]) 
    idlist=record["IdList"]
    search.close()
    webenv = record["WebEnv"] # use with usehistory
    query_key = record["QueryKey"]

    if count >= limit: 
        return jsonify({'warn': 'sorry, query record number beyond %s, please narrow down your search term' % str(limit)})

    # socket.setdefaulttimeout(30) ## timeout 30secs

    @copy_current_request_context
    def entrez_parse(args):
        start, batch_size = args
        papers = []
        try:
            handle = Entrez.efetch(db="pubmed", rettype="medline", retmode="text", retstart = start, retmax=batch_size, webenv=webenv, query_key=query_key) 
            for record in Medline.parse(handle):
                paper = fetch_record((record, IFlist, impact))
                if paper:
                    papers.append(paper)
            handle.close()
        except:
            handle = Entrez.efetch(db="pubmed", rettype="medline", retmode="text", retstart = start, retmax=batch_size, webenv=webenv, query_key=query_key) 
            for record in Medline.parse(handle):
                paper = fetch_record((record, IFlist, impact))
                if paper:
                    papers.append(paper)
            handle.close()
        return papers

    # p = Pool(4)
    batches = range(0, count, batch_size)
    Papers = []

    ##for pr in p.imap(entrez_parse, zip(batches, [batch_size]*len(batches))): ## greenlet concurrency version not stable
    for b in batches: ## no greenlet, stable
        pr = entrez_parse((b, 500))
        if pr:
           Papers += pr

    Papers_sorted=sorted(Papers,key=lambda x:(x['DAF'], x['IF']),reverse=True)
    outMsg = ""
    leftPaper = len(Papers_sorted)

    if str(top).strip().upper() != 'ALL':
        results = Papers_sorted[:int(top)]
##        results = Papers_sorted  ## only for test, comment later
    else:
        results = Papers_sorted

    if not results:
        return jsonify({'warn': 'sorry, no record meet your search parameters'})

## back to the original ncbi link query, though the cited-by number is limited the PMC center, but it's free and less query frequency limitation
    for p in results:
        try:
            citation_handler = Entrez.read(Entrez.elink(dbfrom="pubmed", db="pmc", LinkName="pubmed_pmc_refs", from_uid=p['PMID']))
            pmc_ids = [link["Id"] for link in citation_handler[0]["LinkSetDb"][0]["Link"]]
            p['cite'] = len(pmc_ids)
        except:
            p['cite'] = 0

##    comment the scopus API for citation number of a given paper, useful but limited 
##    of query frequency, better than google scholar
#    for p in results:
#        doi = finddoi(p.get('LID', ''))
#        if doi:
#            p['DOI'] = 'DOI(' + str(doi) + ')'
#            ## no DOI, use pubmed iid
#        else:
#            p['DOI'] = 'PMID(' + str(p['PMID']) + ')'
#
#    DOI = [ p.get('DOI','') for p in results if p.has_key('DOI') ]
#    DOI = '+OR+'.join(DOI)
#    citations = []
#    for start in range(0, len(results), 10):
#        req = urllib2.Request('http://api.elsevier.com/content/search/index:SCOPUS?&start=%s&count=10&query=%s&field=citedby-count,pubmed-id,prism:doi' % (start,DOI))
#        req.add_header('X-ELS-APIKey', '1e5ef7a4e5fa55b80b227711673d9411')
#        opener = urllib2.build_opener()
#        opener.addheaders = [('User-agent', 'Mozilla/5.0')]
#        try:
#            r = opener.open(req)
#            c = r.read()
#            citations += json.loads(c)['search-results']['entry']
#            r.close()
#        except:
#            continue
#
#    try:
#        cite_dict = {}
#        for ci in citations:
#            try:
#                cite_dict[ci['pubmed-id']] = [ci['citedby-count'], ci['prism:url']]
#            except:
#                cite_dict[ci['prism:doi']] = [ci['citedby-count'], ci['prism:url']]
#        for P in results:
#            if cite_dict.has_key(str(P['PMID'])):
#                P['cite'] =  cite_dict[str(P['PMID'])][0]
#                P['scp'] = cite_dict[str(P['PMID'])][1].split(':')[-1]
#
#    except  Exception, e:
#        exstr = traceback.format_exc()
#        print >> sys.stderr, exstr

    outMsg = '\t'.join(['PMID', 'Title', 'Journal', 'First Author', 'Last Author', 'Date', 'Impact Factor', 'Date + Impact Factor', 'Citation']) + '\n'

    for line in results:
        outMsg += '\t'.join([line['PMID'], line['TI'], line['TA'], line['first_author'], line['last_author'], line['DA'], str(line['IF']), str(line['DAF']), str(line['cite'])]) + '\n'
    send_localhost_mail(resultOpt, term, 
                        [str(mail)],
                        render_template('mail.txt', user = mail.split('@')[0], table=outMsg, parameters = parameters), 
                        render_template('mail.html', user=mail.split('@')[0], titles=['PMID', 'Title', 'Journal', 'First Author', 'Last Author', 'Date', 'Impact Factor', 'Date + Impact Factor', 'Cite'], papers=results, parameters = parameters), outMsg)
    current_app.logger.info('|'.join([str(mail), request.remote_addr, term, str(impact), str(top), str(limit), str(count), str(leftPaper), str(time.time() - t0)]))
    return jsonify(result = results, mail=mail, valid=True)

