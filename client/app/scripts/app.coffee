'use strict'

###*
 # @ngdoc overview
 # @name clientApp
 # @description
 # # clientApp
 #
 # Main module of the application.
###
angular
  .module('clientApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ngToast',
    'angular-loading-bar',
    'vcRecaptcha',
    'ui.bootstrap',
    'angular-ladda'
  ])
  .config (cfpLoadingBarProvider) ->
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.includeBar = true;
    cfpLoadingBarProvider.latencyThreshold = 200;
    return
  .config ($httpProvider) ->
    $httpProvider.defaults.headers.common = {}
    $httpProvider.defaults.headers.post = {}
    $httpProvider.defaults.headers.put = {}
    $httpProvider.defaults.headers.patch = {}
    return 
  .config ($stateProvider, $urlRouterProvider) ->
    $urlRouterProvider.otherwise '/index'
    $stateProvider
      .state 'index', {
        url: '',
        views: {
          '': {
            templateUrl: 'views/home.html'
          },
          'search@index': {
            templateUrl: 'views/search.html'
          }    
        }
      }
      .state 'mail', {
        url: '/mail',
        views: {
          '': {
            templateUrl: 'views/home.html'
          },
          'mail@mail': {
            templateUrl: 'views/mail.html'
          }
        } 
      }
      .state 'about', {
        url: '/about',
        views: {
          '': {
            templateUrl: 'views/home.html'
          },
          'about@about': {
            templateUrl: 'views/about.html'
          }
        }    
      }
  .constant "searchUrl", 'http://gnosis.cistrome.org/paper/api'
  .constant "root", 'http://gnosis.cistrome.org/paper/captcha/'
  .constant "rootUrl", 'http://gnosis.cistrome.org'

