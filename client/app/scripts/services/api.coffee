'use strict'

###*
 # @ngdoc service
 # @name clientApp.api
 # @description
 # # api
 # Factory in the clientApp.
###
angular.module('clientApp')
  .factory 'myCaptcha', ($http) ->
    xhr = (url) -> 
        return $http({
            url: url
            method: 'GET'
            headers: {'Content-Type': 'application/json'}
            })
    {
        img: xhr
    }

  .factory 'api', ($http, searchUrl)->


    query = (keywords, mail, impact, topn, limit, gg, hash, resultOpt)-> 
    	$http({
    		method: 'POST'
    		url: searchUrl
    		data: {'keys': keywords, 'email': mail , 'impact': impact, 'top': topn, 'limit': limit, 'recap': gg, 'hash': hash, 'opt': resultOpt} ## recap is the word user input, hash is the hash key in the database
    		headers: {'Content-Type': 'application/json'}
#    		timeout: 80000
    	}).then (resp) ->
            return resp.data
            

    # Public API here
    {
      search: (keywords, mail, impact, topn, limit, gg, hash, resultOpt)->
        query keywords, mail, impact, topn, limit, gg, hash, resultOpt
    }


