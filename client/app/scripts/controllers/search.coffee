'use strict'

###*
 # @ngdoc function
 # @name clientApp.controller:SearchCtrl
 # @description
 # # SearchCtrl
 # Controller of the clientApp
###
angular.module('clientApp').controller 'SearchCtrl', ($scope, $location, $log, ngToast, api, vcRecaptchaService, myCaptcha, root, rootUrl) ->

  $scope.impact = [
    3
    5
    10
    20
  ]
  $scope.tops = [
    100
    200
    300
  ]
  $scope.limits = [
    '10K'
    '5K'
    '2K'
  ]
  $scope.resultOptions = [
    'html'
    'txt attachment'
  ]
  $scope.response = null
  $scope.widgetId = null

  $scope.selectedItem = 100
  $scope.selectedIF = $scope.impact[1]
  $scope.selectedLimit = '10K'
  $scope.resultOption = 'html'


  $scope.hashKey = null
  $scope.refresh_captcha = ()->
    jqxhr = myCaptcha.img(root + 'captcha_refresh/').success (data) ->
      $scope.captcha_img = rootUrl + data.image_url
      $scope.hashKey = data.key;
      return
    .error ()->
      console.log("error refreshing captcha!")

  $scope.validate = () ->
    $scope.text = $scope.captcha_text
    myCaptcha.img(root + 'captcha_validate/' + $scope.hashKey + "/" + $scope.text).success () ->
      ngToast.create(
        content: 'captcha validated'
        class: 'success'
        dismissOnTimeout: true
        dismissButton: true
        dismissOnClick: true
      )    
      return
    .error ()->
      ngToast.create(
        content: 'captcha not validated'
        class: 'error'
        dismissOnTimeout: true
        dismissButton: true
        dismissOnClick: true
      )         
      $scope.refresh()
      return
    return

  $scope.refresh = () ->
    $scope.refresh_captcha()
    return

  $scope.refresh()

  $scope.search = ->
    # $scope.validate()
    $scope.loading = true; 
    apiPromise = api.search($scope.keywords, $scope.email, $scope.selectedIF, $scope.selectedItem, $scope.selectedLimit, $scope.captcha_text, $scope.hashKey, $scope.resultOption)#$scope.response)
    apiPromise.then((data) ->
      valid = true
      if data.hasOwnProperty 'valid'
        valid = data.valid
      if valid
        if data.hasOwnProperty 'warn'
          $scope.text = data.warn
          ngToast.create(
            content: $scope.text
            class: 'error'
            dismissOnTimeout: true
            dismissButton: true
            dismissOnClick: true
          )        
          $scope.refresh()          
          # vcRecaptchaService.reload $scope.widgetId
          return
        else
          #$scope.result = data.result
          #$scope.mail = data.mail  ## for UI only, not necessary now        
          $location.path('/mail')
          #console.log valid
          return        
      else
        ## vcRecaptchaService.reload $scope.widgetId 
        $scope.text = 'captcha not correct, please use it properly'
        $scope.refresh()
        ngToast.create(
          content: $scope.text
          class: 'error'
          dismissOnTimeout: true
          dismissButton: true
          dismissOnClick: true
        )            
        console.log valid
      $scope.loading = false      
      return   
    ).catch((err) ->
      $scope.loading = false
      $log.log(err)
      $scope.refresh()          
      # vcRecaptchaService.reload($scope.widgetId)      
      $scope.keywords = '';
      $scope.email = '';
      ##throw err
      return 
    ).finally () ->
      $scope.loading = false
      console.log 'end'
      return

    $scope.searchHide = false
    return

  # $scope.setResponse = (response) -> 
  #   $scope.response = response
  #   return
  # $scope.setWidgetId = (widgetId) ->
  #   $scope.widgetId = widgetId  
  #   return
  return

# .directive "loadButton", ->
#   (scope, element, attr) ->
#     element.on "click", (e) ->
#       element.button('loading')
