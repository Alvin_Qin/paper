'use strict'

describe 'Controller: MailctrlCtrl', ->

  # load the controller's module
  beforeEach module 'clientApp'

  MailctrlCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    MailctrlCtrl = $controller 'MailctrlCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
